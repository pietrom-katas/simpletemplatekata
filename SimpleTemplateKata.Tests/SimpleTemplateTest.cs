using System.Collections.Generic;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;


namespace SimpleTemplateKata.Tests {
    public class SimpleTemplateTest {
        private readonly TemplateManager target = new TemplateManager();

        [Theory]
        [InlineData("Hello, World!")]
        [InlineData("¡Hola, Pedro!")]
        public void RenderFixedText(string fixedText) {
            var template = target.CompileTemplate(fixedText);
            var result = template.Render(new Dictionary<string, object>());
            Assert.That(result, Is.EqualTo(fixedText));
        }

        [Fact]
        public void RenderTextContainingVariables() {
            var template = target.CompileTemplate("{message}, {to}!");
            var result = template.Render(new Dictionary<string, object> {
                {"message", "Hi"},
                {"to", "pietrom"}
            });
            Assert.That(result, Is.EqualTo("Hi, pietrom!"));
        }

        [Fact]
        public void CanProvideValuesForVariablesWhenCreatingTemplate() {
            var template = target.CompileTemplate("{message}, {to}!", new Dictionary<string, object> {
                {"message", "Hi"},
                {"to", "World"}
            });
            var result = template.Render(new Dictionary<string, object> {
                {"message", "Hi"}
            });
            Assert.That(result, Is.EqualTo("Hi, World!"));
        }

        [Fact]
        public void CanOverwriteDefaultValuesForVariablesAtRenderingLevel() {
            var template = target.CompileTemplate("{message}, {to}!", new Dictionary<string, object> {
                {"message", "Hi"},
                {"to", "World"}
            });
            var result = template.Render(new Dictionary<string, object> {
                {"message", "Hola"},
                {"to", "martinellip"}
            });
            Assert.That(result, Is.EqualTo("Hola, martinellip!"));
        }

        [Fact]
        public void RenderEmptyStringForVariablesWithoutValue() {
            var template = target.CompileTemplate("{message}, {to}!");
            var result = template.Render(new Dictionary<string, object>());
            Assert.That(result, Is.EqualTo(", !"));
        }
        
        [Fact]
        public void CanSpecifyVariableDefaultValuesInTemplate() {
            var template = target.CompileTemplate("{message = 'Hello'}, {to = 'World'}!");
            var result = template.Render(new Dictionary<string, object>());
            Assert.That(result, Is.EqualTo("Hello, World!"));
        }
    }
}