using System.Collections.Generic;

namespace SimpleTemplateKata {
    public abstract class TemplateItem {
        protected TemplateItem(string text) {
            Text = text;
        }

        public string Text { get; }

        public abstract string Render(IDictionary<string, object> data);
    }
}