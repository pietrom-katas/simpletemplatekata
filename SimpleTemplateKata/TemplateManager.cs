using System;
using System.Collections.Generic;

namespace SimpleTemplateKata {
    public class TemplateManager {
        public Template CompileTemplate(string text, IDictionary<string, object> defaultValues) {
            var tpl = new Template(text, defaultValues);
            int openIndex = text.IndexOf("{", StringComparison.InvariantCulture);
            while(openIndex >= 0) {
                tpl.AddSubItem(new FixedItem(text.Substring(0, openIndex)));
                var closedIndex = text.IndexOf("}", openIndex, StringComparison.InvariantCulture);
                var varExpr = text.Substring(openIndex + 1, closedIndex - openIndex - 1);
                var fields = varExpr.Split('=');
                var defaultValue = fields.Length > 1 ? fields[1].Replace("'", string.Empty) : string.Empty;
                tpl.AddSubItem(new VariableItem(varExpr, fields[0].Trim(), defaultValue.Trim()));
                text = text.Substring(closedIndex + 1);
                openIndex = text.IndexOf("{", StringComparison.InvariantCulture);
            }
            tpl.AddSubItem(new FixedItem(text));
            return tpl;
        }
        
        public Template CompileTemplate(string text) {
            return CompileTemplate(text, new Dictionary<string, object>());
        }
    }
}