using System.Collections.Generic;
using System.Linq;

namespace SimpleTemplateKata {
    public static class DictionaryExtension {
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key) {
            return self.ContainsKey(key) ? self[key] : default(TValue);
        }

        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> self,
            IDictionary<TKey, TValue> other) {
            return self
                .Concat(other)
                .GroupBy(x => x.Key)
                .ToDictionary(g => g.Key, g => g.Last().Value);
        }
    }
}