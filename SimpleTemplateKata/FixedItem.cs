using System.Collections.Generic;

namespace SimpleTemplateKata {
    public class FixedItem : TemplateItem {
        public FixedItem(string text) : base(text) {
        }

        public override string Render(IDictionary<string, object> data) {
            return Text;
        }
    }
}