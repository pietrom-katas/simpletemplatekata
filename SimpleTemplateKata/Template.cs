using System.Collections.Generic;
using System.Text;

namespace SimpleTemplateKata {
    public class Template: TemplateItem {
        private readonly IDictionary<string, object> defaultValues;
        private readonly IList<TemplateItem> items;

        public Template(string text, IDictionary<string, object> defaultValues) : base(text) {
            this.defaultValues = defaultValues;
            this.items = new List<TemplateItem>();
        }

        public override string Render(IDictionary<string, object> dictionary) {
            var data = defaultValues.Merge(dictionary);
            var builder = new StringBuilder();
            foreach (var item in items) {
                builder.Append(item.Render(data));
            }

            return builder.ToString();
        }

        public void AddSubItem(TemplateItem item) {
            items.Add(item);
        }
    }
}