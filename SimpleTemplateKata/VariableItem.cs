using System.Collections.Generic;

namespace SimpleTemplateKata {
    public class VariableItem : TemplateItem {
        private readonly string varName;
        private readonly string defaultValue;

        public VariableItem(string text, string varName, string defaultValue) : base(text) {
            this.varName = varName;
            this.defaultValue = defaultValue;
        }

        public override string Render(IDictionary<string, object> data) {
            return (data.GetOrDefault(varName) ?? defaultValue).ToString();
        }
    }
}